#!/bin/sh
# Display manager dependent programs
if [ "$XDG_SESSION_TYPE" == "x11" ]; then
    xrandr --output eDP-1 --mode 2256x1504 --scale 0.8x0.8
    feh --bg-fill $HOME/Pictures/wallpapers/framework/MeerKAT_w_framework.jpeg
    picom &
    ~/.config/polybar/launch_polybar.sh
    setxkbmap -layout ca -option caps:ctrl_modifier
    xset r rate 300 50
    xinput set-prop "PIXA3854:00 093A:0274 Touchpad" "libinput Tapping Enabled" 1
else
    waybar &
fi


#pulseaudio -D
languagetool-http-server --port 8081 --allow-origin "*" &
nm-applet --indicator &
light -S 1
nextcloud --background &
dropbox &
dunst &
protonmail-bridge -n

redshift -t 6500:3000 -l 45.50884:-73.58781 &

$HOME/Documents/Developpement/Logiciels/openrefine-3.7.2/refine &

